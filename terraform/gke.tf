resource "google_container_cluster" "primary" {
  name               = "gitlab-users-cluster"
  location           = "us-central1-a"
  remove_default_node_pool = true
  initial_node_count       = 1

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "gitlab-users-node-pool"
  location   = "us-central1-a"
  cluster    = google_container_cluster.primary.name
  node_count = 1

  node_config {
    preemptible  = true
    machine_type = "e2-medium"

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

resource "helm_release" "ingress_controller" {
  depends_on = [google_container_node_pool.primary_preemptible_nodes]
  name  = "nginx-ingress"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart = "ingress-nginx"
  force_update = "true"
  lint = "true"
}

resource "helm_release" "chartmuseum" {
  depends_on = [google_container_node_pool.primary_preemptible_nodes, helm_release.ingress_controller]
  name  = "chartmuseum"
  repository = "https://kubernetes-charts.storage.googleapis.com"
  chart = "chartmuseum"
  force_update = "true"
  lint = "true"
  
  values = [
    "${file("chartmuseum_values.yml")}"
  ]
  
  set {
    name  = "ingress.hosts[0].name"
    value = "charts.${join("-", split(".", data.kubernetes_service.ingress_endpoint.load_balancer_ingress.0.ip))}.nip.io"
  }
}

data "kubernetes_service" "ingress_endpoint" {
  depends_on = [google_container_node_pool.primary_preemptible_nodes, helm_release.ingress_controller]
  metadata {
    name = "nginx-ingress-ingress-nginx-controller"
  }
}

resource "kubernetes_namespace" "dev_namespace" {
  depends_on = [google_container_node_pool.primary_preemptible_nodes]
  metadata {
    name = "dev"
  }
}

resource "kubernetes_namespace" "prod_namespace" {
  depends_on = [google_container_node_pool.primary_preemptible_nodes]
  metadata {
    name = "prod"
  }
}

locals {
  dockercfg = {
    "registry.gitlab.com" = {
      email    = "name@example.com"
      username = var.gitlab_deploy_username
      password = var.gitlab_deploy_token
    }
  }
}

resource "kubernetes_secret" "gke_gitlab_docker_registry_dev" {
  metadata {
    name = "gke-gitlab-docker-registry"
    namespace = kubernetes_namespace.dev_namespace.id
  }

  data = {
    ".dockerconfigjson" = jsonencode(local.dockercfg)
  }

  type = "kubernetes.io/dockerconfigjson"
}

resource "kubernetes_secret" "gke_gitlab_docker_registry_prod" {
  metadata {
    name = "gke-gitlab-docker-registry"
    namespace = kubernetes_namespace.prod_namespace.id
  }

  data = {
    ".dockerconfigjson" = jsonencode(local.dockercfg)
  }

  type="kubernetes.io/dockerconfigjson"
}