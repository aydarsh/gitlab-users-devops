variable "google_credentials" {
  type = string
}

variable "gitlab_deploy_username" {
  type = string
}

variable "gitlab_deploy_token" {
  type = string
}
