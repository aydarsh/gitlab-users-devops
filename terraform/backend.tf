terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "gitlab-users"

    workspaces {
      prefix = "gitlab-users-devops"
    }
  }
}
