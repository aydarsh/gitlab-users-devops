provider "google" {
  credentials = base64decode(var.google_credentials)
  project     = "gitlab-users"
  region      = "us-central1"
  zone        = "us-central1-a"
}

# Retrieve an access token as the Terraform runner
data "google_client_config" "provider" {}

data "google_container_cluster" "my_cluster" {
  name     = "gitlab-users-cluster"
  location = "us-central1"
}

provider "kubernetes" {
  load_config_file = false

  host  = google_container_cluster.primary.endpoint
  token = data.google_client_config.provider.access_token
  cluster_ca_certificate = base64decode(google_container_cluster.primary.master_auth[0].cluster_ca_certificate)
}

provider "helm" {
  kubernetes {
    host                   = google_container_cluster.primary.endpoint
    cluster_ca_certificate = base64decode(google_container_cluster.primary.master_auth[0].cluster_ca_certificate)
    token                  = data.google_client_config.provider.access_token
    load_config_file       = false
  }
}
